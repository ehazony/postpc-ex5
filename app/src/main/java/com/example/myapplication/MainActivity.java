package com.example.myapplication;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private Button mBtLaunchActivity;
    public String textValue = "press me";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtLaunchActivity = (Button) findViewById(R.id.bt_launch_activity);
        mBtLaunchActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchActivityFisher();
            }
        });
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        textValue = sp.getString("NUMBER_VALUE", "press me");
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(textValue);
        textView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchActivityEditer();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        editor.putString("NUMBER_VALUE", "press me");
        editor.commit();
        super.onSaveInstanceState(savedInstanceState);
    }
    private void launchActivityFisher() {

        Intent intent = new Intent(this, AppActivity.class);
        startActivity(intent);
    }


    private void launchActivityEditer() {

        Intent intent = new Intent(this, AppActivity2.class);
        startActivity(intent);
    }


}
